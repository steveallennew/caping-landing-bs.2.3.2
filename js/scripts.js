(function() {

  var colors = {
    0:   '#fff',
    100: '#34A92F',
    200: '#FA9939',
    300: '#CB6600',
    400: '#FF3010',
    500: '#973501'
  };

  var values = {
    0:   ['0','0','0','0'],
    100: ['1.58','40','8536','16318'],
    200: ['2.06','52','11097','21213'],
    300: ['2.53','64','13657','26108'],
    400: ['3.11','78','16787','32091'],
    500: ['3.69','93','19917','38074']
  };

  $('#zip_in').keypress(function(event){
    var keycode = (event.keyCode ? event.keyCode : event.which);
      if(keycode == '13'){ // Enter key pressed
        $('.pre-onboard').hide();
        $('.post-onboard').show();
        $('.spacer').show();
      }
  });
  var slider = $('#slider');


    slider.slider({
      value:100,
      min: 0,
      max: 500,
      step: 100,
      slide: function( event, ui ) {
        // setColour(ui.value);
        slider.css('background', colors[ui.value]);
        handle.css('background', colors[ui.value]);
        updateText(ui.value);
        amount.text( "£" + ui.value );
      }
    });
    var handle = $('.ui-slider-handle');
    var amount = $('#amount');
    var cards = $('.card-amount');

    var asideOpen = false;

    amount.text( "£" + slider.slider( "value" ) );
    updateText(slider.slider( "value" ));


    function updateText(step){
      cards.each(function(i){
        $(this).text(values[step][i]);
      });
    }

    $('.js-signup').click(function(){
      $('#login_modal').hide();
      $('#signup_modal').fadeIn('fast');
    });

    $('.js-login').click(function(){
      $('#signup_modal').hide();
      $('#login_modal').fadeIn('fast');
    });

    $('.popup_exit').click(function(){
      $('.popup').fadeOut('fast');
    });

    $('.js-aside-toggle').click(function(){
      if (asideOpen) {
        $('.aside').animate({left:'-50%'}, 300);
        asideOpen = false;
      } else {
        $('.aside').animate({left: 0}, 300);
        asideOpen = true;
      }
    });


  })();
